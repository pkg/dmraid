Source: dmraid
Section: admin
Priority: optional
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Standards-Version: 3.9.5
Build-Depends: debhelper (>= 9), libdevmapper-dev, libklibc-dev, libselinux1-dev, quilt (>= 0.40), autotools-dev
Vcs-git: git://git.debian.org/git/users/derevko-guest/dmraid.git
Vcs-Browser: http://git.debian.org/?p=users/derevko-guest/dmraid.git
Homepage: http://people.redhat.com/~heinzm/sw/dmraid/

Package: dmraid
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, udev, dmsetup
Description: Device-Mapper Software RAID support tool
 dmraid discovers, activates, deactivates and displays properties
 of software RAID sets (eg, ATARAID) and contained DOS partitions.
 .
 dmraid uses the Linux device-mapper to create devices with respective
 mappings for the ATARAID sets discovered.
 .
 The following formats are supported: 
  Highpoint HPT37X/HPT45X
  Intel Software RAID
  LSI Logic MegaRAID 
  NVidia NForce RAID (nvraid)
  Promise FastTrack 
  Silicon Image(tm) Medley(tm)
  VIA Software RAID 
 .
 Please read the documentation in /usr/share/doc/dmraid BEFORE attempting
 any use of this software. Improper use can cause data loss!

Package: dmraid-udeb
Architecture: any
Section: debian-installer
XC-Package-Type: udeb
Depends: ${shlibs:Depends}, dmsetup-udeb
Description: Device-Mapper Software RAID support tool (udeb)
 dmraid discovers, activates, deactivates and displays properties
 of software RAID sets (eg, ATARAID) and contained DOS partitions.
 .
 This is the minimal package (udeb) used by debian-installer

Package: libdmraid1.0.0.rc16-udeb
Architecture: any
Section: debian-installer
XC-Package-Type: udeb
Depends: ${shlibs:Depends}
Description: Device-Mapper Software RAID support tool - shared library (udeb)
 dmraid discovers, activates, deactivates and displays properties
 of software RAID sets (eg, ATARAID) and contained DOS partitions.
 .
 This is the minimal package (udeb shared library) used by debian-installer

Package: libdmraid1.0.0.rc16
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Replaces: libdmraid1.0.0.rc15 (<< 1.0.0.rc16-1)
Description: Device-Mapper Software RAID support tool - shared library
 dmraid discovers, activates, deactivates and displays properties
 of software RAID sets (eg, ATARAID) and contained DOS partitions.
 .
 dmraid uses the Linux device-mapper to create devices with respective
 mappings for the ATARAID sets discovered.
 .
 This package contains the dmraid shared library, which implements
 the back half of dmraid, including on-disk metadata formats.

Package: libdmraid-dev
Architecture: any
Section: libdevel
Depends: libdmraid1.0.0.rc16 (= ${binary:Version}), ${misc:Depends}
Description: Device-Mapper Software RAID support tool - header files
 dmraid discovers, activates, deactivates and displays properties
 of software RAID sets (eg, ATARAID) and contained DOS partitions.
 .
 dmraid uses the Linux device-mapper to create devices with respective
 mappings for the ATARAID sets discovered.
 .
 This package contains the header files needed to link programs against
 dmraid.

